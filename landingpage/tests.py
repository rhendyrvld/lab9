from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views, urls

# Create your tests here.
class Lab9Test(TestCase):
    def test_url_landingpage_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_pakai_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_pakai_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
    
    def test_isi_index(self):
        response = Client().get('/')
        isi = response.content.decode('utf8')
        self.assertIn('Hello', isi)

    def test_url_login_ada(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_pakai_template_login(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')